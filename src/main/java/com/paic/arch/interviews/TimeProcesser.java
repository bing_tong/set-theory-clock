package com.paic.arch.interviews;

/**
 * @author shero
 * 处理时间业务逻辑
 */
public interface TimeProcesser {
	void process(Time time);
}
