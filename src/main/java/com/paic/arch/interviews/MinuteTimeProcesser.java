package com.paic.arch.interviews;

public class MinuteTimeProcesser implements TimeProcesser {

	@Override
	public void process(Time time) {
		SegmentProcesser segmentProcesser = new SegmentProcesser() {

			@Override
			public String display(int count) {
				String[] lightArray1 = initLightArray1();
				String[] lightArray2 = initLightArray();
				int number1 = count / 5;
				int number2 = count % 5;
				setLightArray1(lightArray1, number1);
				setLightArray(lightArray2, number2, "Y");
				return parseLightArray(lightArray1, lightArray2);
			}

			private String[] initLightArray1() {
				return new String[] { "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O" };
			}

			private void setLightArray1(String[] lightArray1, int number1) {
				for (int i = 0; i < number1; i++) {
					if (i == 2 || i == 5 || i == 8) {
						lightArray1[i] = "R";
					} else {
						lightArray1[i] = "Y";
					}
				}
			}
		};
		time.setMinuteLight(segmentProcesser.display(segmentProcesser.parse(time.getMinuteSegment())));
	}

}
