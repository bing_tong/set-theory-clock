package com.paic.arch.interviews;

import java.util.ArrayList;
import java.util.List;

public class SampleTimeConverter implements TimeConverter {

	@Override
	public String convertTime(String aTime) {
		Time time = new Time(aTime);
		for(TimeProcesser processer : getProcesserList()) {
			processer.process(time);
		}
		return time.toString();
	}

	private List<TimeProcesser> getProcesserList() {
		List<TimeProcesser> processerList = new ArrayList<TimeProcesser>();
		processerList.add(new HourTimeProcesser());
		processerList.add(new MinuteTimeProcesser());
		processerList.add(new SecondTimeProcesser());
		return processerList;
	}
}
