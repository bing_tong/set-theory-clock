package com.paic.arch.interviews;

/**
 * @author shero 该接口用于抽象时间计算的特征，如小时、分钟、秒钟的计算特征
 */
public abstract class SegmentProcesser {
	/**
	 * @param time
	 *            依照:进行分段所获得的字符型时间
	 * @return 获取整数类型的数值
	 */
	public final int parse(String time) {
		if (time != null && !"".equals(time)) {
			return parseCore(time);
		} else {
			return 0;
		}
	}

	protected int parseCore(String time) {
		return Integer.parseInt(time);
	}

	protected void setLightArray(String[] lightArray, int number, String signal) {
		if (number > lightArray.length) {
			return;
		}
		for (int i = 0; i < number; i++) {
			lightArray[i] = signal;
		}
	}
	
	protected String[] initLightArray() {
		return new String[] {"O", "O", "O", "O"};
	}

	protected String parseLightArray(String[] lightArray1, String[] lightArray2) {
		StringBuilder builder = new StringBuilder();
		for (String light : lightArray1) {
			builder.append(light);
		}
		builder.append("\r\n");
		for (String light : lightArray2) {
			builder.append(light);
		}
		return builder.toString();
	}

	public abstract String display(int count);
}
