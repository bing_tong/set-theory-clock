package com.paic.arch.interviews;

/**
 * @author shero 处理小时时间业务逻辑
 */
public class HourTimeProcesser implements TimeProcesser {

	@Override
	public void process(Time time) {
		SegmentProcesser segmentProcesser = new SegmentProcesser() {

			@Override
			public String display(int count) {
				String[] lightArray1 = initLightArray();
				String[] lightArray2 = initLightArray();
				int number1 = count / 5;
				int number2 = count % 5;
				setLightArray(lightArray1, number1, "R");
				setLightArray(lightArray2, number2, "R");
				return parseLightArray(lightArray1, lightArray2);
			}
		};
		time.setHourLight(segmentProcesser.display(segmentProcesser.parse(time.getHourSegment())));
	}

}
