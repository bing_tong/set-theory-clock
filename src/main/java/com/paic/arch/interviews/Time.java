package com.paic.arch.interviews;

/**
 * @author shero 封装时间处理所需信息
 */
public class Time {
	private String hourSegment;
	private String minuteSegment;
	private String secondSegment;
	private String hourLight;
	private String minuteLight;
	private String secondLight;

	public String getHourLight() {
		return hourLight;
	}

	public void setHourLight(String hourLight) {
		this.hourLight = hourLight;
	}

	public String getMinuteLight() {
		return minuteLight;
	}

	public void setMinuteLight(String minuteLight) {
		this.minuteLight = minuteLight;
	}

	public String getSecondLight() {
		return secondLight;
	}

	public void setSecondLight(String secondLight) {
		this.secondLight = secondLight;
	}

	public Time(String timeStr) {
		String[] segments = timeStr.split(":");
		if (segments.length > 2) {
			secondSegment = segments[2];
		}

		if (segments.length > 1) {
			minuteSegment = segments[1];
		}

		if (segments.length > 0) {
			hourSegment = segments[0];
		}
	}

	public String getHourSegment() {
		return this.hourSegment;
	}

	public String getMinuteSegment() {
		return this.minuteSegment;
	}

	public String getSecondSegment() {
		return this.secondSegment;
	}

	public String toString() {
		return this.getSecondLight() + "\r\n" + this.getHourLight() + "\r\n" + this.getMinuteLight();
	}
}
