package com.paic.arch.interviews;

public class SecondTimeProcesser implements TimeProcesser {

	@Override
	public void process(Time time) {
		SegmentProcesser segmentProcesser = new SegmentProcesser() {

			@Override
			public String display(int count) {
				int number = count % 2;
				if (number == 1) {
					return "O";
				} else {
					return "Y";
				}
			}
		};
		time.setSecondLight(segmentProcesser.display(segmentProcesser.parse(time.getSecondSegment())));
	}

}
